package enoxs.com.app.info;

import java.util.Properties;

public class AppInfoEntity {
    protected AppInfo transform2AppInfo(Properties config){
        AppInfo appInfo = new AppInfo();
        Long id = Long.valueOf(-1);

        try {
            id = Long.valueOf(config.getProperty("app_id"));
        }catch (Exception e){
            e.printStackTrace();
        }

        appInfo.setAppId(id);
        appInfo.setAppName(config.getProperty("app_name"));
        appInfo.setAppVersion(config.getProperty("app_version"));
        appInfo.setAppDate(config.getProperty("app_data"));
        appInfo.setAppAuthor(config.getProperty("app_author"));
        appInfo.setAppRemark(config.getProperty("app_remark"));
        return appInfo;
    }
}
