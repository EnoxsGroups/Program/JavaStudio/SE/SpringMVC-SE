package enoxs.com.app.info;

import enoxs.com.util.ConfigUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;

@Service
public class AppInfoServiceImpl implements AppInfoService {

    @Value("${RESOURCE_ROOT_PATH}")
    private String ressourceRootPath;

    @Value("${APP_INFO_DIR}")
    private String appInfoDir;

    @Value("${APP_INFO_FILE}")
    private String appInfoFile;

    private static Map<Integer, AppInfo> appInfos = null;
    private static AppInfoEntity appInfoEntity = null;

    private void makeAppInfos() {
        System.out.println("init ... ");

        if (appInfoEntity == null) {
            appInfoEntity = new AppInfoEntity();
        }

        if (appInfos == null) {
            appInfos = new HashMap<>();

            AppInfo appInfo;

            String[] fileName = appInfoFile.split(",");
            StringBuffer appInfoPath = new StringBuffer(64);

            for (int i = 0; i < fileName.length; i++) {
                appInfoPath.setLength(0);
                appInfoPath.append(ressourceRootPath);
                appInfoPath.append(File.separator);
                appInfoPath.append(appInfoDir);
                appInfoPath.append(File.separator);
                appInfoPath.append(fileName[i]);

                appInfo = appInfoEntity.transform2AppInfo(ConfigUtil.loadConfigProps(appInfoPath.toString()));

                appInfos.put(i, appInfo);
            }
        }
    }

    @Override
    public AppInfo queryAppInfoByPosition(Integer position) {
        makeAppInfos();

        return appInfos.get(position);
    }

    @Override
    public AppInfo queryAppInfoById(Long id) {
        makeAppInfos();

        AppInfo appInfo = null;
        for (Integer key : appInfos.keySet()) {
            if (id == appInfos.get(key).getAppId()) {
                appInfo = appInfos.get(key);
                break;
            }
        }
        return appInfo;
    }

    @Override
    public AppInfo queryAppInfoByName(String name) {
        makeAppInfos();

        AppInfo appInfo = null;
        for (Integer key : appInfos.keySet()) {
            if (name.equals(appInfos.get(key).getAppName())) {
                appInfo = appInfos.get(key);
                break;
            }
        }

        return appInfo;
    }

    @Override
    public AppInfo queryAppInfoByIdAndName(Long id, String name) {
        makeAppInfos();

        AppInfo appInfo = null;
        for (Integer key : appInfos.keySet()) {
            if (id == appInfos.get(key).getAppId() && name.equals(appInfos.get(key).getAppName())) {
                appInfo = appInfos.get(key);
                break;
            }
        }
        return appInfo;
    }

    @Override
    public List<AppInfo> queryAppInfoByMultiPostion(Integer[] positions) {
        makeAppInfos();

        List<AppInfo> lstAppInfo = new LinkedList<>();
        AppInfo appInfo = null;

        for (Integer position : positions) {
            appInfo = appInfos.get(position);
            lstAppInfo.add(appInfo);
        }
        return lstAppInfo;
    }

    @Override
    public List<AppInfo> queryAppInfoByMultiName(String[] names) {
        makeAppInfos();

        List<AppInfo> lstAppInfo = new LinkedList<>();
        AppInfo appInfo = null;

        List<String> lstName = Arrays.asList(names);

        for (Integer key : appInfos.keySet()) {
            appInfo = appInfos.get(key);
            if(lstName.contains(appInfo.getAppName())){
                lstAppInfo.add(appInfo);
            }
        }

        return lstAppInfo;
    }

    @Override
    public List<AppInfo> queryAppInfoByMultiId(List lstId) {
        makeAppInfos();

        List<AppInfo> lstAppInfo = new LinkedList<>();
        AppInfo appInfo = null;

        for (Integer key : appInfos.keySet()) {
            appInfo = appInfos.get(key);
            if(lstId.contains(appInfo.getAppId())){
                lstAppInfo.add(appInfo);
            }
        }

        return lstAppInfo;
    }

    @Override
    public AppInfo queryAppInfoByAppInfoObject(AppInfo appInfo) {
        makeAppInfos();

        AppInfo mAppInfo = null;
        for (Integer key : appInfos.keySet()) {
            mAppInfo = appInfos.get(key);
            if(mAppInfo.getAppId().equals(appInfo.getAppId()) &&
                    mAppInfo.getAppName().equals(appInfo.getAppName()) &&
                    mAppInfo.getAppVersion().equals(appInfo.getAppVersion()) &&
                    mAppInfo.getAppDate().equals(appInfo.getAppDate()) &&
                    mAppInfo.getAppAuthor().equals(appInfo.getAppAuthor())
                ){
                break;
            }
        }
        return mAppInfo;
    }
}
