Java Web Application Spring MVC - Simple Example
======

![dashboard](assets/readme/dashboard.png)

專案說明
------
Java Web Application Spring MVC 框架，簡易參考範例。

demo: <https://rhzenoxs.github.io/SpringMVC-SE/>
![demo-view](assets/readme/demo-view.png)

整併 Spring + MyBatis 企業應用實戰（第2版）學習時的範例程式：
+ <https://github.com/RHZEnoxs/WebAppX-v1.0.1>
+ <https://github.com/RHZEnoxs/WebAppX-v1.0.2>
+ <https://github.com/RHZEnoxs/WebAppX-v1.0.3>

目錄說明
------

### Java 主程式
dir: src/enoxs/com/

### Web 網頁
dir: web/

### 單元測試 (test )
dir: test/enoxs/com/
        
### 使用手冊 (doc)
dir: doc/enoxs/com/


操作環境
------
+ IDE : IntelliJ
+ SDK : OpenJDK8U
+ Tomcat : Tomcat 8.5.33